import {
  GetProductsForIngredient,
  GetRecipes,
} from "./supporting-files/data-access";
import {
  NutrientFact,
  Product,
  Recipe,
  RecipeLineItem,
} from "./supporting-files/models";
import {
  GetCostPerBaseUnit,
  GetNutrientFactInBaseUnits,
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

/**
 * TASK:
 * - to build a functionality that will calculate the cheapest cost that each recipe can be made for
 * - to summarise its nutritional information
 */

interface LeastCostAndNutrients {
  leastCost: number;
  nutrients: NutrientFact[];
}

// a class that will represent the Recipe's summary and all involved process
class RecipeSummary {
  private cheapestCost: number = 0;
  private nutrientFacts: NutrientFact[] = [];

  constructor(public recipe: Recipe) {
    this.getRecipeSummary(recipe.lineItems);
  }

  // loop through the ingredients and get the total of all cheapest ingredients
  // as well as its nutritional facts
  private getRecipeSummary(ingredients: RecipeLineItem[]): void {
    for (const item of ingredients) {
      const { leastCost, nutrients } = this.getLeastCostAndNutrients(item);
      this.cheapestCost += leastCost;
      this.nutrientFacts = this.nutrientFacts.concat(nutrients);
    }
  }

  // get only the least cost item and its nutrient facts
  private getLeastCostAndNutrients(
    item: RecipeLineItem
  ): LeastCostAndNutrients {
    const products: Product[] = GetProductsForIngredient(item.ingredient);

    // get only the least cost item and its nutrient facts
    let leastCost: number = 0;
    let nutrients: NutrientFact[] = [];

    for (let product of products) {
      for (let supplier of product.supplierProducts) {
        const currentCost = GetCostPerBaseUnit(supplier);
        if (currentCost < leastCost || leastCost === 0) {
          leastCost = currentCost;
          nutrients = product.nutrientFacts.map((nth) =>
            GetNutrientFactInBaseUnits(nth)
          );
        }
      }
    }

    return {
      leastCost: item.unitOfMeasure.uomAmount * leastCost,
      nutrients,
    };
  }

  // summarize nutrient facts by adding all uomAmount of duplicate nutrients
  public getNutrientFacts(): Record<string, NutrientFact> {
    return this.nutrientFacts.reduce(
      (output: Record<string, NutrientFact>, nutrient: NutrientFact) => {
        if (output[nutrient.nutrientName])
          output[nutrient.nutrientName].quantityAmount.uomAmount +=
            nutrient.quantityAmount.uomAmount;
        else output[nutrient.nutrientName] = nutrient;
        return output;
      },
      {}
    );
  }

  public getCheapestCost(): number {
    return this.cheapestCost;
  }

  // return data in expected format that matches the test sample
  public toJSON() {
    return {
      cheapestCost: this.getCheapestCost(),
      nutrientsAtCheapestCost: Object.fromEntries(
        Object.entries(this.getNutrientFacts()).sort()
      ),
    };
  }
}

for (const recipe of recipeData) {
  const summary = new RecipeSummary(recipe);
  summary;
  recipeSummary[recipe.recipeName] = summary.toJSON();
}

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
